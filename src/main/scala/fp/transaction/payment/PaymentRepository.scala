package fp.transaction.payment

import fp.basic.doobie.{
  paymentMeta,
  statusMeta,
  BasicDoobieRepository,
  BasicEntitySQLAlgebra
}
import fp.domain.Transaction.Payment
import fp.domain.validation.basic.instances.payment._

final class PaymentRepository()
    extends BasicDoobieRepository(PaymentRepository.PaymentSQL)

object PaymentRepository {
  def apply(): PaymentRepository = new PaymentRepository()

  private[payment] object PaymentSQL extends BasicEntitySQLAlgebra[Payment] {
    import doobie.syntax.string._
    import doobie.{Query0, Update0}

    def insert(payment: Payment): Update0 = {
      import payment._
      sql"""INSERT INTO payments (id, from_id, to_id, amount, type, status)
            VALUES ($id, $fromId, $toId, $amount, ${`type`}, $status)
            """.update
    }

    def selectById(id: Payment#Id): Query0[Payment] =
      sql"""SELECT id, from_id, to_id, amount, type, status
            FROM payments
            WHERE id = $id
            """.query

    def selectAll: Query0[Payment] =
      sql"""SELECT id, from_id, to_id, amount, type, status
            FROM payments
            """.query

    def update(id: Payment#Id, payment: Payment): Update0 =
      sql"""UPDATE payments
            SET status = ${payment.status}
            WHERE id = $id
            """.update

    def delete(id: Payment#Id): Update0 =
      sql"""DELETE FROM payments WHERE id = $id
            """.update
  }
}
