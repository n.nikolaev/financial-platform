package fp.transaction.payment

import cats.data.EitherT
import cats.syntax.bifunctor._
import cats.syntax.flatMap._
import doobie.ConnectionIO
import fp.account.AccountServiceAlgebra
import fp.basic.doobie.BasicDoobieService
import fp.domain.BasicRepositoryAlgebra
import fp.domain.PaymentStatus._
import fp.domain.Transaction.Payment
import fp.domain.validation.ValidationError
import fp.domain.validation.payment._
import fp.domain.validation.syntax._
import fp.transaction.trustlink.TrustLinkDoobieService

class PaymentDoobieService(
    repository: BasicRepositoryAlgebra[ConnectionIO, Payment],
    accountService: AccountServiceAlgebra[ConnectionIO],
    trustLinkService: TrustLinkDoobieService
) extends BasicDoobieService(repository) {

  override def create(payment: Payment): Int |! ConnectionIO =
    validate(payment) >>
      checkRestrictions(payment) >> {
        payment.status match {
          case Done =>
            performMoneyTransfer(payment) >> repository.create(payment)
          case _    => repository.create(payment)
        }
      }

  override def update(
      id: Payment#Id,
      f: Payment => Payment |! ConnectionIO
  ): Int |! ConnectionIO =
    repository.update(
      id,
      paymentBeforeUpdate => {
        f(paymentBeforeUpdate).flatMap { paymentAfterUpdate =>
          validate(paymentAfterUpdate) >>
            checkRestrictions(paymentAfterUpdate) >> {
              (paymentBeforeUpdate.status, paymentAfterUpdate.status) match {
                case (_, Pending)    =>
                  EitherT.leftT(PaymentUpdateToPendingError)
                case (Pending, Done) =>
                  performMoneyTransfer(paymentAfterUpdate).map { _ =>
                    paymentAfterUpdate
                  }
                case (Pending, _)    =>
                  EitherT.pure(paymentAfterUpdate)
                case _               =>
                  EitherT.leftT(PaymentAlreadyCompletedError)
              }
            }
        }
      }
    )

  override def validate(payment: Payment): Unit |! ConnectionIO =
    if (payment.amount.compare(0) < 1)
      EitherT.leftT(PaymentNonPositiveAmountError)
    else
      for {
        _ <- accountService.getById(payment.fromId)
        _ <- accountService
               .getById(payment.toId)
               .leftWiden[ValidationError]
      } yield ()

  private def checkRestrictions(
      payment: Payment
  ): Unit |! ConnectionIO =
    trustLinkService
      .getSet(payment.toId)
      .flatMap { fromIds =>
        EitherT.cond(
          fromIds.isEmpty ||
            fromIds.contains(payment.fromId),
          (),
          PaymentRestrictionError(payment)
        )
      }

  private def performMoneyTransfer(
      payment: Payment
  ): Int |! ConnectionIO =
    accountService.decreaseBalance(payment.fromId, payment.amount) >>
      accountService.increaseBalance(payment.toId, payment.amount)
}

object PaymentDoobieService {
  def apply(
      repository: BasicRepositoryAlgebra[ConnectionIO, Payment],
      accountService: AccountServiceAlgebra[ConnectionIO],
      trustLinkService: TrustLinkDoobieService
  ): PaymentDoobieService =
    new PaymentDoobieService(
      repository,
      accountService,
      trustLinkService
    )
}
