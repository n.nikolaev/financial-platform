package fp.transaction.history

import doobie.{ConnectionIO, Query0}
import doobie.syntax.string._
import fp.basic.doobie.{paymentMeta, statusMeta}
import fp.domain.Account
import fp.domain.Transaction._

abstract sealed class TransactionHistoryRepository {
  import TransactionHistoryRepository.HistorySQL

  def getTransfersFromId(fromId: Account#Id): ConnectionIO[List[Transfer]] =
    HistorySQL.getTransfersFromId(fromId).to[List]

  def getTransfersToId(toId: Account#Id): ConnectionIO[List[Transfer]] =
    HistorySQL.getTransfersToId(toId).to[List]

  def getPaymentsFromId(fromId: Account#Id): ConnectionIO[List[Payment]] =
    HistorySQL.getPaymentsFromId(fromId).to[List]

  def getPaymentsToId(toId: Account#Id): ConnectionIO[List[Payment]] =
    HistorySQL.getPaymentsToId(toId).to[List]
}

object TransactionHistoryRepository {
  def apply(): TransactionHistoryRepository =
    new TransactionHistoryRepository() {}

  private object HistorySQL {
    def getTransfersFromId(fromId: Account#Id): Query0[Transfer] =
      sql"""SELECT id, from_id, to_id, amount
            FROM transfers
            WHERE from_id = $fromId
            """.query[Transfer]

    def getTransfersToId(toId: Account#Id): Query0[Transfer] =
      sql"""SELECT id, from_id, to_id, amount
            FROM transfers
            WHERE to_id = $toId
            """.query[Transfer]

    def getPaymentsFromId(fromId: Account#Id): Query0[Payment] =
      sql"""SELECT id, from_id, to_id, amount, type, status
            FROM payments
            WHERE from_id = $fromId
            """.query[Payment]

    def getPaymentsToId(toId: Account#Id): Query0[Payment] = {
      sql"""SELECT id, from_id, to_id, amount, type, status
            FROM payments
            WHERE to_id = $toId
            """.query[Payment]
    }
  }
}
