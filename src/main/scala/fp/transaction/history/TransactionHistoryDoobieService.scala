package fp.transaction.history

import cats.data.EitherT
import cats.syntax.flatMap._
import doobie.ConnectionIO
import fp.account.AccountServiceAlgebra
import fp.domain.Account
import fp.domain.Transaction._
import fp.domain.validation.syntax._

class TransactionHistoryDoobieService(
    repository: TransactionHistoryRepository,
    accountService: AccountServiceAlgebra[ConnectionIO]
) {
  def getTransfersFromId(
      fromId: Account#Id
  ): List[Transfer] |? ConnectionIO =
    validatedQuery(fromId, repository.getTransfersFromId)

  def getTransfersToId(
      toId: Account#Id
  ): List[Transfer] |? ConnectionIO =
    validatedQuery(toId, repository.getTransfersToId)

  def getPaymentsFromId(
      fromId: Account#Id
  ): List[Payment] |? ConnectionIO =
    validatedQuery(fromId, repository.getPaymentsFromId)

  def getPaymentsToId(
      toId: Account#Id
  ): List[Payment] |? ConnectionIO =
    validatedQuery(toId, repository.getPaymentsToId)

  private def validatedQuery[A](
      id: Account#Id,
      repositoryQuery: Account#Id => ConnectionIO[List[A]]
  ): List[A] |? ConnectionIO =
    accountService.getById(id) >> EitherT.liftF(repositoryQuery(id))
}

object TransactionHistoryDoobieService {
  def apply(
      repository: TransactionHistoryRepository,
      accountService: AccountServiceAlgebra[ConnectionIO]
  ): TransactionHistoryDoobieService =
    new TransactionHistoryDoobieService(repository, accountService)
}
