package fp.transaction.history

import cats.data.EitherT
import cats.effect.Sync
import cats.syntax.semigroupal._
import doobie.ConnectionIO
import doobie.syntax.connectionio._
import doobie.util.transactor.Transactor
import fp.domain.Transaction._
import fp.domain.validation.ValidationError.NotFoundError
import fp.domain.validation.syntax._
import io.circe.syntax.EncoderOps
import io.circe.{Codec, Json, JsonObject}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl

final class TransactionHistoryEndpoint[F[_]: Sync](
    service: TransactionHistoryDoobieService,
    xa: Transactor[F]
)(implicit
    transferCodec: Codec[Transfer],
    paymentCodec: Codec[Payment]
) {

  import TransactionHistoryEndpoint.names._

  val dsl: Http4sDsl[F] = new Http4sDsl[F] {}
  import dsl._
  import org.http4s.circe.CirceEntityCodec._

  val routes: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root / tpe / direction / id =>
      val serviceResponse: Json |? F =
        (tpe, direction) match {
          case (TRANSFERS, FROM) => service.getTransfersFromId(id).toJson
          case (TRANSFERS, TO)   => service.getTransfersToId(id).toJson
          case (PAYMENTS, FROM)  => service.getPaymentsFromId(id).toJson
          case (PAYMENTS, TO)    => service.getPaymentsToId(id).toJson
          case (ALL, FROM)       =>
            service
              .getTransfersFromId(id)
              .product(service.getPaymentsFromId(id))
              .toJson
          case (ALL, TO)         =>
            service
              .getTransfersToId(id)
              .product(service.getPaymentsToId(id))
              .toJson
          case _                 => EitherT.leftT(NotFoundError(""))
        }

      serviceResponse.foldF(
        notFoundError => NotFound(notFoundError.message),
        serviceResponseJson =>
          Ok(
            JsonObject.singleton(
              "history",
              Json.fromJsonObject(
                JsonObject(
                  "id"        -> id.asJson,
                  "direction" -> direction.asJson,
                  tpe         -> serviceResponseJson
                )
              )
            )
          )
      )
  }

  private implicit class ListToJsonOrNotFoundOps[A: Codec](
      listOrNotFound: List[A] |? ConnectionIO
  ) {
    def toJson: Json |? F =
      listOrNotFound.map(_.asJson).transact(xa)
  }

  private implicit class TwoListsToJsonOrNotFoundOps(
      transactionsOrNotFound: (List[Transfer], List[Payment]) |? ConnectionIO
  ) {
    def toJson: Json |? F =
      transactionsOrNotFound
        .map { case (transfers, payments) =>
          Json.fromJsonObject(
            JsonObject(
              TRANSFERS -> transfers.asJson,
              PAYMENTS  -> payments.asJson
            )
          )
        }
        .transact(xa)
  }
}

object TransactionHistoryEndpoint {
  def apply[F[_]: Sync](
      service: TransactionHistoryDoobieService,
      xa: Transactor[F]
  )(implicit
      transferCodec: Codec[Transfer],
      paymentCodec: Codec[Payment]
  ): TransactionHistoryEndpoint[F] =
    new TransactionHistoryEndpoint(service, xa)

  private object names {
    val TRANSFERS: String = "transfers"
    val PAYMENTS: String  = "payments"

    val FROM: String = "from"
    val TO: String   = "to"
    val ALL: String  = "all"
  }
}
