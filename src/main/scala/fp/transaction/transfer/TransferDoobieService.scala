package fp.transaction.transfer

import cats.data.EitherT
import cats.syntax.bifunctor._
import cats.syntax.flatMap._
import doobie.ConnectionIO
import fp.account.AccountServiceAlgebra
import fp.basic.doobie.BasicDoobieService
import fp.domain.BasicRepositoryAlgebra
import fp.domain.Transaction.Transfer
import fp.domain.validation.ValidationError
import fp.domain.validation.syntax._
import fp.domain.validation.transfer.TransferNonPositiveAmountError

class TransferDoobieService(
    repository: BasicRepositoryAlgebra[ConnectionIO, Transfer],
    accountService: AccountServiceAlgebra[ConnectionIO]
) extends BasicDoobieService(repository) {

  override def create(transfer: Transfer): Int |! ConnectionIO =
    validate(transfer) >>
      accountService.decreaseBalance(transfer.fromId, transfer.amount) >>
      accountService.increaseBalance(transfer.toId, transfer.amount) >>
      repository.create(transfer)

  override def validate(transfer: Transfer): Unit |! ConnectionIO = {
    if (transfer.amount.compare(0) < 1)
      EitherT.leftT(TransferNonPositiveAmountError)
    else
      for {
        _ <- accountService.getById(transfer.fromId)
        _ <- accountService
               .getById(transfer.toId)
               .leftWiden[ValidationError]
      } yield ()
  }
}

object TransferDoobieService {
  def apply(
      repository: BasicRepositoryAlgebra[ConnectionIO, Transfer],
      accountService: AccountServiceAlgebra[ConnectionIO]
  ): TransferDoobieService =
    new TransferDoobieService(repository, accountService)
}
