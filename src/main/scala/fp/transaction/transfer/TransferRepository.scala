package fp.transaction.transfer

import fp.basic.doobie.{BasicDoobieRepository, BasicEntitySQLAlgebra}
import fp.domain.Transaction.Transfer
import fp.domain.validation.basic.instances.transfer._

final class TransferRepository()
    extends BasicDoobieRepository(TransferRepository.TransferSQL)

object TransferRepository {
  def apply(): TransferRepository = new TransferRepository()

  private[transfer] object TransferSQL extends BasicEntitySQLAlgebra[Transfer] {
    import doobie.syntax.string._
    import doobie.{Query0, Update0}

    def insert(transfer: Transfer): Update0 = {
      import transfer._
      sql"""INSERT INTO transfers (id, from_id, to_id, amount)
            VALUES ($id, $fromId, $toId, $amount)
            """.update
    }

    def selectById(id: Transfer#Id): Query0[Transfer] =
      sql"""SELECT id, from_id, to_id, amount
            FROM transfers
            WHERE id = $id
            """.query

    def selectAll: Query0[Transfer] =
      sql"""SELECT id, from_id, to_id, amount
            FROM transfers
            """.query

    def update(id: Transfer#Id, transfer: Transfer): Update0 =
      sql"""UPDATE transfers
            SET amount = ${transfer.amount}
            WHERE id = $id
            """.update

    def delete(id: Transfer#Id): Update0 =
      sql"""DELETE FROM transfers WHERE id = $id
            """.update
  }
}
