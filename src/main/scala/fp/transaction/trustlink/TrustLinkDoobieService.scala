package fp.transaction.trustlink

import cats.data.EitherT
import cats.syntax.flatMap._
import doobie.ConnectionIO
import fp.domain.validation.syntax._
import fp.domain.{Account, BasicServiceAlgebra, TrustLink}

class TrustLinkDoobieService(
    repository: TrustLinkRepository,
    accountService: BasicServiceAlgebra[ConnectionIO, Account]
) {
  def create(
      trustLink: TrustLink
  ): Int |? ConnectionIO =
    accountService.getById(trustLink.fromId) >>
      accountService.getById(trustLink.toId) >>
      EitherT.liftF(repository.create(trustLink))

  def getSet(
      toId: Account#Id
  ): Set[Account#Id] |? ConnectionIO =
    accountService.getById(toId) >>
      EitherT.liftF(repository.getSet(toId))

  def delete(
      trustLink: TrustLink
  ): Int |? ConnectionIO =
    repository.delete(trustLink)
}

object TrustLinkDoobieService {
  def apply(
      repository: TrustLinkRepository,
      accountService: BasicServiceAlgebra[ConnectionIO, Account]
  ): TrustLinkDoobieService =
    new TrustLinkDoobieService(repository, accountService)
}
