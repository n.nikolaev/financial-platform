package fp.transaction.trustlink

import cats.data.EitherT
import cats.syntax.applicative._
import doobie.ConnectionIO
import fp.domain.validation.syntax._
import fp.domain.validation.trustlink.TrustLinkNotFoundError
import fp.domain.{Account, TrustLink}

abstract sealed class TrustLinkRepository {
  import TrustLinkRepository.TrustLinkSQL

  def create(trustLink: TrustLink): ConnectionIO[Int] =
    getSet(trustLink.toId).flatMap { fromIds =>
      if (fromIds contains trustLink.fromId)
        0.pure[ConnectionIO]
      else
        TrustLinkSQL.insert(trustLink).run
    }

  def getSet(toId: Account#Id): ConnectionIO[Set[Account#Id]] =
    TrustLinkSQL.selectAllById(toId).to[Set]

  def delete(trustLink: TrustLink): Int |? ConnectionIO =
    EitherT.liftF(TrustLinkSQL.delete(trustLink).run).flatMap { affectedRows =>
      EitherT.cond(
        affectedRows != 0,
        affectedRows,
        TrustLinkNotFoundError(trustLink)
      )
    }
}

object TrustLinkRepository {
  def apply(): TrustLinkRepository =
    new TrustLinkRepository() {}

  private object TrustLinkSQL {
    import doobie.syntax.string._
    import doobie.{Query0, Update0}

    def insert(trustLink: TrustLink): Update0 =
      sql"""INSERT INTO trust_links (from_id, to_id)
            VALUES (${trustLink.fromId}, ${trustLink.toId})
            """.update

    def selectAllById(toId: Account#Id): Query0[String] =
      sql"""SELECT from_id
            FROM trust_links
            WHERE to_id = $toId
            """.query

    def delete(trustLink: TrustLink): Update0 =
      sql"""DELETE FROM trust_links
            WHERE from_id = ${trustLink.fromId}
            AND to_id = ${trustLink.toId}
            """.update
  }
}
