package fp.transaction.trustlink

import cats.effect.Sync
import cats.syntax.flatMap._
import doobie.syntax.connectionio._
import doobie.util.transactor.Transactor
import fp.domain.{Account, TrustLink}
import io.circe.syntax.EncoderOps
import io.circe.{Codec, JsonObject}
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Response}

class TrustLinkEndpoint[F[_]: Sync](
    service: TrustLinkDoobieService,
    xa: Transactor[F]
)(implicit codec: Codec[TrustLink]) {
  val dsl: Http4sDsl[F] = new Http4sDsl[F] {}
  import dsl._
  import org.http4s.circe.CirceEntityCodec._

  final val routes: HttpRoutes[F] = HttpRoutes.of[F] {
    case req @ POST -> Root      => req.as[TrustLink].flatMap(create)
    case GET -> Root / accountId => getSet(accountId)

    case DELETE -> Root / accountId / trustedAccountId =>
      delete(TrustLink(accountId, trustedAccountId))
  }

  private def create(trustLink: TrustLink): F[Response[F]] =
    service
      .create(trustLink)
      .transact(xa)
      .foldF(
        notFoundError => NotFound(notFoundError.message),
        _ => Created()
      )

  private def getSet(toId: Account#Id): F[Response[F]] =
    service
      .getSet(toId)
      .transact(xa)
      .foldF(
        notFoundError => NotFound(notFoundError.message),
        trustLinksSet => Ok(JsonObject("trust_links" -> trustLinksSet.asJson))
      )

  private def delete(trustLink: TrustLink): F[Response[F]] =
    service
      .delete(trustLink)
      .transact(xa)
      .foldF(
        notFoundError => NotFound(notFoundError.message),
        _ => NoContent()
      )
}

object TrustLinkEndpoint {
  def apply[F[_]: Sync](
      service: TrustLinkDoobieService,
      xa: Transactor[F]
  )(implicit codec: Codec[TrustLink]): TrustLinkEndpoint[F] =
    new TrustLinkEndpoint(service, xa)
}
