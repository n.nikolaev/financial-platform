package fp.transaction

import cats.effect.Sync
import doobie.ConnectionIO
import doobie.util.transactor.Transactor
import fp.account.{AccountModule, AccountServiceAlgebra}
import fp.basic.doobie.BasicDoobieEndpoint
import fp.domain.Transaction.{Payment, Transfer}
import fp.domain.{BasicServiceAlgebra, TrustLink}
import fp.transaction.history._
import fp.transaction.payment._
import fp.transaction.transfer._
import fp.transaction.trustlink._
import org.http4s.HttpRoutes
import org.http4s.server.Router

class TransactionModule[F[_]: Sync](
    accountModule: AccountModule[F],
    xa: Transactor[F]
) {
  import TransactionModule._

  private val accountService: AccountServiceAlgebra[ConnectionIO] =
    accountModule.accountService

  val historyService: TransactionHistoryDoobieService =
    TransactionHistoryDoobieService(
      TransactionHistoryRepository(),
      accountService
    )

  val trustLinkService: TrustLinkDoobieService =
    TrustLinkDoobieService(
      TrustLinkRepository(),
      accountService
    )

  val paymentService: BasicServiceAlgebra[ConnectionIO, Payment] =
    PaymentDoobieService(
      PaymentRepository(),
      accountService,
      trustLinkService
    )

  val transferService: BasicServiceAlgebra[ConnectionIO, Transfer] =
    TransferDoobieService(
      TransferRepository(),
      accountService
    )

  private val historyEndpoint: TransactionHistoryEndpoint[F] =
    TransactionHistoryEndpoint(historyService, xa)

  private val paymentEndpoint: BasicDoobieEndpoint[F, Payment] =
    BasicDoobieEndpoint(paymentService, xa)

  private val transferEndpoint: BasicDoobieEndpoint[F, Transfer] =
    BasicDoobieEndpoint(transferService, xa)

  private val trustLinkEndpoint: TrustLinkEndpoint[F] =
    TrustLinkEndpoint(trustLinkService, xa)

  val routes: HttpRoutes[F] = Router(
    "/history"     -> historyEndpoint.routes,
    "/payments"    -> paymentEndpoint.routes,
    "/transfers"   -> transferEndpoint.routes,
    "/trust-links" -> trustLinkEndpoint.routes
  )
}

object TransactionModule {
  def apply[F[_]: Sync](
      accountModule: AccountModule[F],
      xa: Transactor[F]
  ): TransactionModule[F] =
    new TransactionModule(accountModule, xa)

  import io.circe.Codec
  import io.circe.generic.semiauto.deriveCodec

  implicit val paymentCirceCodec: Codec[Payment]     = deriveCodec
  implicit val transferCirceCodec: Codec[Transfer]   = deriveCodec
  implicit val trustLinkCirceCodec: Codec[TrustLink] = deriveCodec
}
