package fp.basic

import cats.effect.Sync
import cats.syntax.flatMap._
import fp.domain.HasId
import io.circe.Codec
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, Response}

abstract class BasicEndpointAlgebra[F[_]: Sync, A <: HasId: Codec] {
  final protected val dsl: Http4sDsl[F] = new Http4sDsl[F] {}
  import dsl._
  import org.http4s.circe.CirceEntityCodec._

  final val routes: HttpRoutes[F] = HttpRoutes.of[F] {
    case req @ POST -> Root     => req.as[A].flatMap(create)
    case GET -> Root / id       => getById(id.asInstanceOf[A#Id])
    case GET -> Root            => list()
    case req @ PUT -> Root / id =>
      req.as[A].flatMap(a => update(id.asInstanceOf[A#Id], a))
    case DELETE -> Root / id    => delete(id.asInstanceOf[A#Id])
  }

  protected def create(entity: A): F[Response[F]]
  protected def getById(id: A#Id): F[Response[F]]
  protected def list(): F[Response[F]]
  protected def update(id: A#Id, entity: A): F[Response[F]]
  protected def delete(id: A#Id): F[Response[F]]
}
