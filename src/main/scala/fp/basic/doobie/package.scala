package fp.basic

import _root_.doobie.Meta
import fp.domain.{PaymentStatus, PaymentType}

package object doobie {
  implicit val paymentMeta: Meta[PaymentType] =
    Meta[String].timap(PaymentType.withName)(_.entryName)

  implicit val statusMeta: Meta[PaymentStatus] =
    Meta[String].timap(PaymentStatus.withName)(_.entryName)
}
