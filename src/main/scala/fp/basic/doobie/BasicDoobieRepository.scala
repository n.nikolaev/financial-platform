package fp.basic.doobie

import cats.data.EitherT
import doobie.{ConnectionIO, Read}
import fp.domain.validation.basic.BasicErrors
import fp.domain.validation.basic.syntax._
import fp.domain.validation.syntax._
import fp.domain.{BasicRepositoryAlgebra, HasId}

abstract class BasicDoobieRepository[A <: HasId: BasicErrors: Read](
    entitySQL: BasicEntitySQLAlgebra[A]
) extends BasicRepositoryAlgebra[ConnectionIO, A] {
  override def create(entity: A): Int |! ConnectionIO =
    getById(entity.id).biflatMap(
      _ => EitherT.liftF(entitySQL.insert(entity).run),
      _ => EitherT.leftT(entity.id.alreadyExists)
    )

  override def getById(id: A#Id): A |? ConnectionIO =
    EitherT.fromOptionF(entitySQL.selectById(id).option, id.notFound)

  override def list: ConnectionIO[List[A]] =
    entitySQL.selectAll.to[List]

  override def update(
      id: A#Id,
      f: A => A |! ConnectionIO
  ): Int |! ConnectionIO =
    getByIdForUpdate(id).flatMap { entityFromDB =>
      f(entityFromDB).flatMap { entityToUpdate =>
        EitherT.liftF(entitySQL.update(id, entityToUpdate).run)
      }
    }

  override def delete(id: A#Id): Int |? ConnectionIO =
    EitherT.liftF(entitySQL.delete(id).run).flatMap { affectedRows =>
      EitherT.cond(affectedRows != 0, affectedRows, id.notFound)
    }

  override def getByIdForUpdate(id: A#Id): A |? ConnectionIO =
    EitherT.fromOptionF(entitySQL.selectForUpdate(id).option, id.notFound)
}
