package fp.basic.doobie

import cats.effect.Sync
import cats.syntax.flatMap._
import doobie.ConnectionIO
import doobie.syntax.connectionio._
import doobie.util.transactor.Transactor
import fp.basic.BasicEndpointAlgebra
import fp.domain.validation.ValidationError._
import fp.domain.{BasicServiceAlgebra, HasId}
import io.circe.syntax.EncoderOps
import io.circe.{Codec, JsonObject}
import org.http4s.Response

class BasicDoobieEndpoint[F[_]: Sync, A <: HasId: Codec](
    service: BasicServiceAlgebra[ConnectionIO, A],
    xa: Transactor[F]
) extends BasicEndpointAlgebra[F, A] {
  import dsl._
  import org.http4s.circe.CirceEntityCodec._

  override protected def create(entity: A): F[Response[F]] =
    service
      .create(entity)
      .transact(xa)
      .foldF(
        {
          case NotFoundError(message) => NotFound(message)
          case NotValidError(message) => BadRequest(message)
        },
        _ => Created()
      )

  override protected def getById(id: A#Id): F[Response[F]] =
    service
      .getById(id)
      .transact(xa)
      .foldF(
        notFoundError => NotFound(notFoundError.message),
        entity => Ok(entity)
      )

  override protected def list(): F[Response[F]] =
    service.list.transact(xa).flatMap { list =>
      Ok(JsonObject("response" -> list.asJson))
    }

  override protected def update(id: A#Id, entity: A): F[Response[F]] =
    service
      .updatePure(id, _ => entity)
      .transact(xa)
      .foldF(
        {
          case NotFoundError(message) => NotFound(message)
          case NotValidError(message) => BadRequest(message)
        },
        _ => Accepted()
      )

  override protected def delete(id: A#Id): F[Response[F]] =
    service
      .delete(id)
      .transact(xa)
      .foldF(
        notFoundError => NotFound(notFoundError.message),
        _ => NoContent()
      )
}

object BasicDoobieEndpoint {
  def apply[F[_]: Sync, A <: HasId: Codec](
      service: BasicServiceAlgebra[ConnectionIO, A],
      xa: Transactor[F]
  ): BasicDoobieEndpoint[F, A] =
    new BasicDoobieEndpoint(service, xa)
}
