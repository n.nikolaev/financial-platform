package fp.basic.doobie

import doobie.syntax.string._
import doobie.{Query0, Read, Update0}
import fp.domain.HasId

trait BasicEntitySQLAlgebra[A <: HasId] {
  def insert(entity: A): Update0
  def selectById(id: A#Id): Query0[A]
  def selectAll: Query0[A]
  def update(id: A#Id, entity: A): Update0
  def delete(id: A#Id): Update0

  def selectForUpdate(id: A#Id)(implicit readEv: Read[A]): Query0[A] =
    sql"""${selectById(id).toFragment}
          FOR UPDATE
          """.query
}
