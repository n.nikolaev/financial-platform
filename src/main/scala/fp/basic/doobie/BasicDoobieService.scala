package fp.basic.doobie

import cats.data.EitherT
import cats.syntax.flatMap._
import doobie.ConnectionIO
import fp.domain.validation.syntax._
import fp.domain.{BasicRepositoryAlgebra, BasicServiceAlgebra, HasId}

class BasicDoobieService[A <: HasId](
    repository: BasicRepositoryAlgebra[ConnectionIO, A]
) extends BasicServiceAlgebra[ConnectionIO, A] {

  override def create(entity: A): Int |! ConnectionIO =
    validate(entity) >> repository.create(entity)

  override def getById(id: A#Id): A |? ConnectionIO =
    repository.getById(id)

  override def list: ConnectionIO[List[A]] =
    repository.list

  override def update(
      id: A#Id,
      f: A => A |! ConnectionIO
  ): Int |! ConnectionIO =
    repository.update(
      id,
      entity =>
        for {
          entityToUpdate <- f(entity)
          _              <- validate(entityToUpdate)
        } yield entityToUpdate
    )

  override def delete(id: A#Id): Int |? ConnectionIO =
    repository.delete(id)

  override def updatePure(
      id: A#Id,
      f: A => A
  ): Int |! ConnectionIO =
    update(id, a => EitherT.pure(f(a)))

  protected def validate(entity: A): Unit |! ConnectionIO =
    EitherT.pure(())
}

object BasicDoobieService {
  def apply[A <: HasId](
      repository: BasicRepositoryAlgebra[ConnectionIO, A]
  ): BasicDoobieService[A] =
    new BasicDoobieService(repository)
}
