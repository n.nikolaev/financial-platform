package fp

import cats.effect._
import doobie.hikari.HikariTransactor
import doobie.util.ExecutionContexts
import fp.account.AccountModule
import fp.config._
import fp.transaction.TransactionModule
import fp.user.UserModule
import io.circe.config.parser
import org.http4s.HttpApp
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.{Router, Server => H4Server}
import org.http4s.syntax.kleisli._

object Server extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    createServer[IO].use(_ => IO.never) as ExitCode.Success

  def createServer[F[_]: ContextShift: ConcurrentEffect: Timer]
      : Resource[F[*], H4Server[F]] = for {
    serverEC <- ExecutionContexts.cachedThreadPool
    conf     <- Resource.liftF(parser.decodePathF[F, AppConfig]("app"))
    xa       <- createTransactor(conf)
    httpApp   = createHttpApp(xa)
    server   <- BlazeServerBuilder(serverEC)
                  .bindHttp(conf.server.port, conf.server.host)
                  .withHttpApp(httpApp)
                  .resource
  } yield server

  def createTransactor[F[_]: Async: ContextShift](
      config: AppConfig
  ): Resource[F[*], HikariTransactor[F]] = for {
    connEC <- ExecutionContexts.fixedThreadPool(
                config.db.connections.poolSize
              )
    txnEC  <- ExecutionContexts.cachedThreadPool
    xa     <- HikariTransactor.newHikariTransactor(
                config.db.driver,
                config.db.url,
                config.db.username,
                config.db.password,
                connEC,
                Blocker.liftExecutionContext(txnEC)
              )
  } yield xa

  def createHttpApp[F[_]: Sync](xa: HikariTransactor[F]): HttpApp[F] = {
    val userModule        = UserModule(xa)
    val accountModule     = AccountModule(userModule, xa)
    val transactionModule = TransactionModule(accountModule, xa)

    Router(
      "/users"        -> userModule.routes,
      "/accounts"     -> accountModule.routes,
      "/transactions" -> transactionModule.routes
    ).orNotFound
  }
}
