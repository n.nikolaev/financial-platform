package fp.user

import fp.basic.doobie.{BasicDoobieRepository, BasicEntitySQLAlgebra}
import fp.domain.User
import fp.domain.validation.basic.instances.user._

final class UserRepository()
    extends BasicDoobieRepository(UserRepository.UserSQL)

object UserRepository {
  def apply(): UserRepository = new UserRepository()

  private[user] object UserSQL extends BasicEntitySQLAlgebra[User] {
    import doobie.syntax.string._
    import doobie.{Query0, Update0}

    def insert(user: User): Update0 =
      sql"""INSERT INTO users (id, email)
            VALUES (${user.id}, ${user.email})
            """.update

    def selectById(id: User#Id): Query0[User] =
      sql"""SELECT id, email
            FROM users
            WHERE id = $id
            """.query

    def selectAll: Query0[User] =
      sql"""SELECT id, email
            FROM users
            """.query

    def update(id: User#Id, user: User): Update0 =
      sql"""UPDATE users
            SET email = ${user.email}
            WHERE id = $id
            """.update

    def delete(id: User#Id): Update0 =
      sql"""DELETE FROM users WHERE id = $id
            """.update
  }
}
