package fp.user

import cats.effect.Sync
import doobie.ConnectionIO
import doobie.util.transactor.Transactor
import fp.basic.doobie.{BasicDoobieEndpoint, BasicDoobieService}
import fp.domain.{BasicServiceAlgebra, User}
import org.http4s.HttpRoutes

class UserModule[F[_]: Sync](xa: Transactor[F]) {
  import UserModule.userCirceCodec

  val userService: BasicServiceAlgebra[ConnectionIO, User] =
    BasicDoobieService(UserRepository())

  private val endpoint: BasicDoobieEndpoint[F, User] =
    BasicDoobieEndpoint(userService, xa)

  val routes: HttpRoutes[F] = endpoint.routes
}

object UserModule {
  def apply[F[_]: Sync](xa: Transactor[F]): UserModule[F] =
    new UserModule(xa)

  import io.circe.Codec
  import io.circe.generic.semiauto.deriveCodec

  implicit val userCirceCodec: Codec[User] = deriveCodec
}
