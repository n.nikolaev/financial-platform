package fp.account

import cats.effect.Sync
import doobie.ConnectionIO
import doobie.util.transactor.Transactor
import fp.basic.doobie.BasicDoobieEndpoint
import fp.domain.Account
import fp.user.UserModule
import org.http4s.HttpRoutes

class AccountModule[F[_]: Sync](
    userModule: UserModule[F],
    xa: Transactor[F]
) {
  import AccountModule.accountCirceCodec

  val accountService: AccountServiceAlgebra[ConnectionIO] =
    AccountDoobieService(AccountRepository(), userModule.userService)

  private val endpoint: BasicDoobieEndpoint[F, Account] =
    BasicDoobieEndpoint(accountService, xa)

  val routes: HttpRoutes[F] = endpoint.routes
}

object AccountModule {
  def apply[F[_]: Sync](
      userModule: UserModule[F],
      xa: Transactor[F]
  ): AccountModule[F] =
    new AccountModule(userModule, xa)

  import io.circe.Codec
  import io.circe.generic.semiauto.deriveCodec

  implicit val accountCirceCodec: Codec[Account] = deriveCodec
}
