package fp.account

import cats.data.EitherT
import cats.syntax.bifunctor._
import doobie.ConnectionIO
import fp.basic.doobie.BasicDoobieService
import fp.domain.validation.ValidationError
import fp.domain.validation.account.NegativeAccountBalanceError
import fp.domain.validation.syntax._
import fp.domain.{Account, BasicRepositoryAlgebra, BasicServiceAlgebra, User}

class AccountDoobieService(
    repository: BasicRepositoryAlgebra[ConnectionIO, Account],
    userService: BasicServiceAlgebra[ConnectionIO, User]
) extends BasicDoobieService(repository)
    with AccountServiceAlgebra[ConnectionIO] {

  override def increaseBalance(
      id: Account#Id,
      amount: BigDecimal
  ): Int |! ConnectionIO =
    changeAccountBalance(id, amount)

  override def decreaseBalance(
      id: Account#Id,
      amount: BigDecimal
  ): Int |! ConnectionIO =
    changeAccountBalance(id, -amount)

  private def changeAccountBalance(
      id: Account#Id,
      delta: BigDecimal
  ): Int |! ConnectionIO =
    updatePure(
      id,
      acc => acc.copy(balance = acc.balance + delta)
    )

  override def validate(account: Account): Unit |! ConnectionIO =
    if (account.balance.compare(0) < 0)
      EitherT.leftT(NegativeAccountBalanceError)
    else
      userService
        .getById(account.userId)
        .leftWiden[ValidationError]
        .map(_ => ())
}

object AccountDoobieService {
  def apply(
      repository: BasicRepositoryAlgebra[ConnectionIO, Account],
      userService: BasicServiceAlgebra[ConnectionIO, User]
  ): AccountDoobieService =
    new AccountDoobieService(repository, userService)
}
