package fp.account

import fp.domain.validation.syntax._
import fp.domain.{Account, BasicServiceAlgebra}

trait AccountServiceAlgebra[F[_]] extends BasicServiceAlgebra[F, Account] {
  def increaseBalance(id: Account#Id, amount: BigDecimal): Int |! F
  def decreaseBalance(id: Account#Id, amount: BigDecimal): Int |! F
}
