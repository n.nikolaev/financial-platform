package fp.account

import fp.basic.doobie.{BasicDoobieRepository, BasicEntitySQLAlgebra}
import fp.domain.Account
import fp.domain.validation.basic.instances.account._

final class AccountRepository()
    extends BasicDoobieRepository(AccountRepository.AccountSQL) {}

object AccountRepository {
  def apply(): AccountRepository = new AccountRepository()

  private[account] object AccountSQL extends BasicEntitySQLAlgebra[Account] {
    import doobie.syntax.string._
    import doobie.{Query0, Update0}

    def insert(account: Account): Update0 =
      sql"""INSERT INTO accounts (id, user_id, balance)
            VALUES (${account.id}, ${account.userId}, ${account.balance})
            """.update

    def selectById(id: Account#Id): Query0[Account] =
      sql"""SELECT id, user_id, balance
            FROM accounts
            WHERE id = $id
            """.query

    def selectAll: Query0[Account] =
      sql"""SELECT id, user_id, balance
            FROM accounts
            """.query

    def update(id: Account#Id, account: Account): Update0 =
      sql"""UPDATE accounts
            SET balance = ${account.balance}
            WHERE id = $id
            """.update

    def delete(id: Account#Id): Update0 =
      sql"""DELETE FROM accounts WHERE id = $id
            """.update
  }
}
