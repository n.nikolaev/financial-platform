package fp.domain

import fp.domain.validation.syntax._

trait BasicRepositoryAlgebra[F[_], A <: HasId] {
  def create(entity: A): Int |! F
  def getById(id: A#Id): A |? F
  def list: F[List[A]]
  def update(id: A#Id, f: A => A |! F): Int |! F
  def delete(id: A#Id): Int |? F

  def getByIdForUpdate(id: A#Id): A |? F
}
