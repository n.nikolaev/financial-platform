package fp

import enumeratum._

package object domain {
  trait HasId {
    type Id
    def id: Id
  }

  final case class User(id: String, email: String) extends HasId {
    override type Id = String
  }

  final case class Account(
      id: String,
      userId: User#Id,
      balance: BigDecimal
  ) extends HasId {
    override type Id = String
  }

  sealed abstract class Transaction extends HasId with Product with Serializable

  object Transaction {
    final case class Transfer(
        id: String,
        fromId: Account#Id,
        toId: Account#Id,
        amount: BigDecimal
    ) extends Transaction
        with HasId {
      override type Id = String
    }

    final case class Payment(
        id: String,
        fromId: Account#Id,
        toId: Account#Id,
        amount: BigDecimal,
        `type`: PaymentType,
        status: PaymentStatus
    ) extends Transaction
        with HasId {
      override type Id = String
    }
  }

  sealed abstract class PaymentType
      extends EnumEntry
      with Product
      with Serializable
  case object PaymentType
      extends Enum[PaymentType]
      with CirceEnum[PaymentType] {
    final case object Purchase    extends PaymentType
    final case object Debt        extends PaymentType
    final case object Fundraising extends PaymentType

    val values: IndexedSeq[PaymentType] = findValues
  }

  sealed abstract class PaymentStatus
      extends EnumEntry
      with Product
      with Serializable
  case object PaymentStatus
      extends Enum[PaymentStatus]
      with CirceEnum[PaymentStatus] {
    final case object Pending  extends PaymentStatus
    final case object Done     extends PaymentStatus
    final case object Rejected extends PaymentStatus
    final case object Canceled extends PaymentStatus

    val values: IndexedSeq[PaymentStatus] = findValues
  }

  final case class TrustLink(
      fromId: Account#Id,
      toId: Account#Id
  )
}
