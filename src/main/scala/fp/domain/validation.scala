package fp.domain

object validation {
  sealed trait ValidationError extends Product with Serializable {
    def message: String
  }
  object ValidationError {
    final case class NotFoundError(message: String) extends ValidationError
    final case class NotValidError(message: String) extends ValidationError
  }

  object syntax {
    import cats.data.EitherT

    /**
      *    Type alias for cases where F can contain either A or a NotFoundError
      */
    type |?[A, F[_]] = EitherT[F, ValidationError.NotFoundError, A]

    /**
      *    Type alias for cases where F can contain either A or a ValidationError
      */
    type |![A, F[_]] = EitherT[F, ValidationError, A]
  }

  object basic {
    import ValidationError._

    trait BasicErrors[A <: HasId] {
      def notFound(id: A#Id): NotFoundError
      def alreadyExists(id: A#Id): NotValidError
    }

    object BasicErrors {
      def apply[A <: HasId](implicit ev: BasicErrors[A]): BasicErrors[A] = ev
    }

    object syntax {
      implicit class BasicErrorsEntityIdOps[A <: HasId: BasicErrors](id: A#Id) {
        def notFound: NotFoundError      = BasicErrors[A].notFound(id)
        def alreadyExists: NotValidError = BasicErrors[A].alreadyExists(id)
      }

      implicit class BasicErrorsEntityOps[A <: HasId: BasicErrors](entity: A) {
        def notFound: NotFoundError      =
          BasicErrors[A].notFound(entity.id)
        def alreadyExists: NotValidError =
          BasicErrors[A].alreadyExists(entity.id)
      }
    }

    object instances {
      import fp.domain.Transaction._

      object user {
        implicit val userBasicErrors: BasicErrors[User] =
          new BasicErrors[User] {
            override def notFound(id: User#Id): NotFoundError =
              validation.user.UserNotFoundError(id)

            override def alreadyExists(id: User#Id): NotValidError =
              validation.user.UserAlreadyExistsError(id)
          }
      }

      object account {
        implicit val accountBasicErrors: BasicErrors[Account] =
          new BasicErrors[Account] {
            override def notFound(id: Account#Id): NotFoundError =
              validation.account.AccountNotFoundError(id)

            override def alreadyExists(id: Account#Id): NotValidError =
              validation.account.AccountAlreadyExistsError(id)
          }
      }

      object transfer {
        implicit val transferBasicErrors: BasicErrors[Transfer] =
          new BasicErrors[Transfer] {
            override def notFound(id: Transfer#Id): NotFoundError =
              validation.transfer.TransferNotFoundError(id)

            override def alreadyExists(id: Transfer#Id): NotValidError =
              validation.transfer.TransferAlreadyExistsError(id)
          }
      }

      object payment {
        implicit val paymentBasicErrors: BasicErrors[Payment] =
          new BasicErrors[Payment] {
            override def notFound(id: Payment#Id): NotFoundError =
              validation.payment.PaymentNotFoundError(id)

            override def alreadyExists(id: Payment#Id): NotValidError =
              validation.payment.PaymentAlreadyExistsError(id)
          }
      }
    }
  }

  object user {
    import ValidationError._

    object UserNotFoundError {
      def apply(id: User#Id): NotFoundError =
        NotFoundError(s"User with id $id not found")
    }

    object UserAlreadyExistsError {
      def apply(id: User#Id): NotValidError =
        NotValidError(s"User with id $id already exists")
    }
  }

  object account {
    import ValidationError._

    object AccountNotFoundError {
      def apply(id: Account#Id): NotFoundError =
        NotFoundError(s"Account with id $id not found")
    }

    object AccountAlreadyExistsError {
      def apply(id: Account#Id): NotValidError =
        NotValidError(s"Account with id $id already exists")
    }

    val NegativeAccountBalanceError: NotValidError =
      NotValidError("Account balance mustn't be negative")
  }

  object trustlink {
    import ValidationError._

    object TrustLinkNotFoundError {
      def apply(trustLink: TrustLink): NotFoundError =
        NotFoundError(s"$trustLink not found")
    }
  }

  object transfer {
    import ValidationError._
    import fp.domain.Transaction.Transfer

    object TransferNotFoundError {
      def apply(id: Transfer#Id): NotFoundError =
        NotFoundError(s"Transfer with id $id not found")
    }

    object TransferAlreadyExistsError {
      def apply(id: Transfer#Id): NotValidError =
        NotValidError(s"Transfer with id $id already exists")
    }

    val TransferNonPositiveAmountError: NotValidError =
      NotValidError("Transfer amount must be a positive number")
  }

  object payment {
    import ValidationError._
    import fp.domain.Transaction.Payment

    object PaymentNotFoundError {
      def apply(id: Payment#Id): NotFoundError =
        NotFoundError(s"Payment with id $id not found")
    }

    object PaymentAlreadyExistsError {
      def apply(id: Payment#Id): NotValidError =
        NotValidError(s"Payment with id $id already exists")
    }

    val PaymentNonPositiveAmountError: NotValidError =
      NotValidError("Payment amount must be a positive number")

    object PaymentRestrictionError {
      def apply(payment: Payment): NotValidError =
        NotValidError(
          s"Payment transaction to account with id ${payment.toId} is not allowed"
        )
    }

    val PaymentAlreadyCompletedError: NotValidError =
      NotValidError("Status update of a completed payment is not allowed")

    val PaymentUpdateToPendingError: NotValidError =
      NotValidError(s"Update to 'Pending' status is not allowed")
  }
}
