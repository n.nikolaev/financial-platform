package fp.domain

import fp.domain.validation.syntax._

trait BasicServiceAlgebra[F[_], A <: HasId] {
  def create(entity: A): Int |! F
  def getById(id: A#Id): A |? F
  def list: F[List[A]]
  def update(id: A#Id, f: A => A |! F): Int |! F
  def delete(id: A#Id): Int |? F

  def updatePure(id: A#Id, f: A => A): Int |! F
}
