package fp

import io.circe.Decoder
import io.circe.generic.semiauto.deriveDecoder

package object config {
  final case class AppConfig(server: ServerConfig, db: DatabaseConfig)
  final case class ServerConfig(host: String, port: Int)

  final case class DatabaseConfig(
      driver: String,
      url: String,
      username: String,
      password: String,
      connections: DBConnectionsConfig
  )

  final case class DBConnectionsConfig(poolSize: Int)

  implicit val appDecoder: Decoder[AppConfig]               = deriveDecoder
  implicit val serverDecoder: Decoder[ServerConfig]         = deriveDecoder
  implicit val dbDecoder: Decoder[DatabaseConfig]           = deriveDecoder
  implicit val dbConnsDecoder: Decoder[DBConnectionsConfig] = deriveDecoder
}
