CREATE TABLE IF NOT EXISTS users (
  id        VARCHAR NOT NULL PRIMARY KEY,
  email     VARCHAR NOT NULL
);

CREATE TABLE IF NOT EXISTS accounts (
  id        VARCHAR NOT NULL PRIMARY KEY,
  user_id   VARCHAR NOT NULL REFERENCES users(id),
  balance   INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS trust_links (
  from_id   VARCHAR NOT NULL REFERENCES accounts(id),
  to_id     VARCHAR NOT NULL REFERENCES accounts(id),
  PRIMARY KEY (from_id, to_id)
);

CREATE TABLE IF NOT EXISTS transfers (
  id        VARCHAR NOT NULL PRIMARY KEY,
  from_id   VARCHAR NOT NULL REFERENCES accounts(id),
  to_id     VARCHAR NOT NULL REFERENCES accounts(id),
  amount    INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS payments (
  id        VARCHAR NOT NULL PRIMARY KEY,
  from_id   VARCHAR NOT NULL REFERENCES accounts(id),
  to_id     VARCHAR NOT NULL REFERENCES accounts(id),
  amount    INTEGER NOT NULL,
  type      VARCHAR NOT NULL,
  status    VARCHAR NOT NULL
);

ANALYZE;
