INSERT INTO users VALUES ('u1', 'email1');
INSERT INTO users VALUES ('u2', 'email2');
INSERT INTO users VALUES ('u3', 'email3');
INSERT INTO users VALUES ('u4', 'email4');
INSERT INTO users VALUES ('u5', 'email5');

INSERT INTO accounts VALUES ('acc1', 'u1', 1000);
INSERT INTO accounts VALUES ('acc2', 'u2', 2000);
INSERT INTO accounts VALUES ('acc3', 'u3', 1500);
INSERT INTO accounts VALUES ('acc4', 'u4', 2000);
INSERT INTO accounts VALUES ('acc5', 'u5', 1000);

INSERT INTO transfers VALUES ('tr1', 'acc2', 'acc1', 500);
INSERT INTO transfers VALUES ('tr2', 'acc4', 'acc5', 300);
INSERT INTO transfers VALUES ('tr3', 'acc3', 'acc5', 200);

COMMIT;
