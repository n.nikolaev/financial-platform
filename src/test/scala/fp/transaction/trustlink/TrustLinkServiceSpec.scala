package fp.transaction.trustlink

import cats.effect.IO
import doobie.Transactor
import doobie.syntax.connectionio._
import fp.DoobieServiceTestUtils
import fp.domain.TrustLink
import fp.domain.validation.account._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

final class TrustLinkServiceSpec
    extends AnyFunSuite
    with Matchers
    with BeforeAndAfterAll
    with TrustLinkServiceTestUtils {

  private val xa: Transactor[IO] = DoobieServiceTestUtils.rollbackTransactor

  override def beforeAll(): Unit =
    initTrustLinks().unsafeRunSync()

  test("create a new trust link") {
    val tl = TrustLink("acc3", "acc4")
    trustLinkService
      .create(tl)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Right(1)
      }
      .unsafeRunSync()
  }

  test("create a trust link from a non-existing account") {
    val tl = TrustLink("unknown_acc_id1", "acc5")
    trustLinkService
      .create(tl)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          AccountNotFoundError(tl.fromId)
        )
      }
      .unsafeRunSync()
  }

  test("create a trust link to a non-existing account") {
    val tl = TrustLink("acc3", "unknown_acc_id2")
    trustLinkService
      .create(tl)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          AccountNotFoundError(tl.toId)
        )
      }
      .unsafeRunSync()
  }

  test("get a non empty set of trust links") {
    val accId = "acc2"
    trustLinkService
      .getSet(accId)
      .transact(xa)
      .value
      .map { set =>
        set.map { actualLinks =>
          testTrustLinksMap.get(accId).map { expectedLinks =>
            actualLinks should contain theSameElementsAs expectedLinks
          }
        }
      }
      .unsafeRunSync()
  }

  test("get an empty set of trust links") {
    trustLinkService
      .getSet("acc4")
      .transact(xa)
      .value
      .map { set =>
        set.map { actualLinks =>
          actualLinks shouldEqual Set.empty[String]
        }
      }
      .unsafeRunSync()
  }

  test("get trust links for non-existing account") {
    val unknownAccId = "unknown_acc_id3"
    trustLinkService
      .getSet(unknownAccId)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          AccountNotFoundError(unknownAccId)
        )
      }
  }

  test("delete an existing trust link") {
    trustLinkService
      .delete(testTrustLinks.head)
      .transact(xa)
      .value
      .map(_ shouldEqual Right(1))
      .unsafeRunSync()
  }
}
