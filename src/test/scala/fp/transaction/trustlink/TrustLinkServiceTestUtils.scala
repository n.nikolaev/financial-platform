package fp.transaction.trustlink

import cats.effect.IO
import cats.syntax.flatMap._
import cats.syntax.traverse._
import doobie.syntax.connectionio._
import fp.DoobieServiceTestUtils.transactorIO
import fp.account.AccountServiceTestUtils
import fp.domain.TrustLink

trait TrustLinkServiceTestUtils extends AccountServiceTestUtils {

  protected final val testTrustLinks = List(
    TrustLink("acc1", "acc2"),
    TrustLink("acc2", "acc1"),
    TrustLink("acc5", "acc2"),
    TrustLink("acc5", "acc3")
  )

  protected final val testTrustLinksMap: Map[String, Set[String]] =
    testTrustLinks.toSet.groupMap[String, String](_.toId)(_.fromId)

  protected final val trustLinkService: TrustLinkDoobieService =
    TrustLinkDoobieService(TrustLinkRepository(), accountService)

  protected final def initTrustLinks(): IO[Unit] =
    initAccounts() >>
      testTrustLinks
        .traverse(trustLinkService.create)
        .transact(transactorIO)
        .value
        .void
}
