package fp.transaction.payment

import cats.effect.IO
import cats.syntax.bifunctor._
import cats.syntax.flatMap._
import cats.syntax.semigroupal._
import doobie.syntax.connectionio._
import doobie.{ConnectionIO, Transactor}
import fp.DoobieServiceTestUtils
import fp.domain.Account
import fp.domain.PaymentStatus._
import fp.domain.PaymentType._
import fp.domain.Transaction.Payment
import fp.domain.validation.account._
import fp.domain.validation.payment._
import fp.domain.validation.syntax._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

final class PaymentServiceSpec
    extends AnyFunSuite
    with Matchers
    with BeforeAndAfterAll
    with PaymentServiceTestUtils {

  private val xa: Transactor[IO] = DoobieServiceTestUtils.rollbackTransactor

  override def beforeAll(): Unit =
    initPayments().unsafeRunSync()

  test("create a new payment") {
    val payment =
      Payment("toCreate", "acc2", "acc1", 700, Purchase, Done)

    def getAccounts:  (Account, Account) |! ConnectionIO =
      accountService
        .getById(payment.fromId)
        .product(accountService.getById(payment.toId))
        .leftWiden

    (
      for {
        (fromAccBefore, toAccBefore) <- getAccounts
        _                            <- paymentService.create(payment)
        (fromAccAfter, toAccAfter)   <- getAccounts
      } yield {
        (
          fromAccBefore.balance - fromAccAfter.balance,
          toAccAfter.balance - toAccBefore.balance
        )
      }
    ).transact(xa)
      .value
      .map {
        _.map { case (fromAccDelta, toAccDelta) =>
          fromAccDelta shouldEqual payment.amount
          toAccDelta shouldEqual payment.amount
        }
      }
      .unsafeRunSync()
  }

  test("create a payment with existing id") {
    val payment = testPayments.head
    paymentService
      .create(payment)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          PaymentAlreadyExistsError(payment.id)
        )
      }
      .unsafeRunSync()
  }

  test("create a transfer with a negative amount") {
    val payment = Payment("toCreate", "acc2", "acc5", -100, Debt, Canceled)
    paymentService
      .create(payment)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          PaymentNonPositiveAmountError
        )
      }
      .unsafeRunSync()
  }

  test("create a payment from a non-existing account") {
    val payment =
      Payment("toCreate", "unknown_id", "acc5", 900, Debt, Pending)
    paymentService
      .create(payment)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          AccountNotFoundError(payment.fromId)
        )
      }
      .unsafeRunSync()
  }

  test("create a payment to a non-existing account") {
    val payment =
      Payment("toCreate", "acc2", "unknown_id", 900, Fundraising, Done)
    paymentService
      .create(payment)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          AccountNotFoundError(payment.toId)
        )
      }
      .unsafeRunSync()
  }

  test("get an existing payment") {
    val payment = testPayments(1)
    paymentService
      .getById(payment.id)
      .transact(xa)
      .value
      .map(_ shouldEqual Right(payment))
      .unsafeRunSync()
  }

  test("get a non-existing payment") {
    paymentService
      .getById("unknown_id")
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          PaymentNotFoundError("unknown_id")
        )
      }
      .unsafeRunSync()
  }

  test("get all payments") {
    paymentService.list
      .transact(xa)
      .map {
        _ should contain theSameElementsAs testPayments
      }
      .unsafeRunSync()
  }

  test("update an existing payment") {
    val payment        = testPayments(2)
    val updatedPayment = payment.copy(status = Rejected)

    (
      paymentService.updatePure(payment.id, _ => updatedPayment) >>
        paymentService.getById(updatedPayment.id).leftWiden
    )
      .transact(xa)
      .value
      .map {
        _ shouldEqual Right(updatedPayment)
      }
      .unsafeRunSync()
  }

  test("update a non-existing payment") {
    val unknownId = "toUpdate"

    paymentService
      .updatePure(unknownId, identity)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          PaymentNotFoundError(unknownId)
        )
      }
      .unsafeRunSync()
  }

  test("update status to 'Pending'") {
    val payment = testPayments(3)

    paymentService
      .updatePure(payment.id, _.copy(status = Pending))
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          PaymentUpdateToPendingError
        )
      }
      .unsafeRunSync()
  }

  test("update status of an already completed payment") {
    val payment = testPayments(3)

    paymentService
      .updatePure(payment.id, _.copy(status = Canceled))
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          PaymentAlreadyCompletedError
        )
      }
      .unsafeRunSync()
  }

  test("delete an existing payment") {
    paymentService
      .delete(testPayments(3).id)
      .transact(xa)
      .value
      .map(_ shouldEqual Right(1))
      .unsafeRunSync()
  }
}
