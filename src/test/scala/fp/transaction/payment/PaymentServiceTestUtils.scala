package fp.transaction.payment

import cats.effect.IO
import cats.syntax.flatMap._
import cats.syntax.traverse._
import doobie.syntax.connectionio._
import fp.DoobieServiceTestUtils.transactorIO
import fp.domain.PaymentStatus._
import fp.domain.PaymentType._
import fp.domain.Transaction.Payment
import fp.transaction.trustlink.TrustLinkServiceTestUtils

trait PaymentServiceTestUtils extends TrustLinkServiceTestUtils {

  protected final val testPayments: List[Payment] = List(
    Payment("p1", "acc2", "acc1", 100, Debt, Done),
    Payment("p2", "acc1", "acc2", 100, Fundraising, Done),
    Payment("p3", "acc3", "acc5", 200, Purchase, Pending),
    Payment("p4", "acc5", "acc3", 200, Fundraising, Rejected)
  )

  protected final val paymentService =
    PaymentDoobieService(
      PaymentRepository(),
      accountService,
      trustLinkService
    )

  protected final def initPayments(): IO[Unit] =
    initTrustLinks() >>
      testPayments
        .traverse(paymentService.create)
        .transact(transactorIO)
        .value
        .void
}
