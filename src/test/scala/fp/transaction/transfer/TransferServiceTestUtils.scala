package fp.transaction.transfer

import cats.effect.IO
import cats.syntax.flatMap._
import cats.syntax.traverse._
import doobie.syntax.connectionio._
import fp.DoobieServiceTestUtils.transactorIO
import fp.account.AccountServiceTestUtils
import fp.domain.Transaction.Transfer

trait TransferServiceTestUtils extends AccountServiceTestUtils {

  protected final val testTransfers: List[Transfer] = List(
    Transfer("tr1", "acc2", "acc1", 500),
    Transfer("tr2", "acc4", "acc2", 500),
    Transfer("tr3", "acc5", "acc4", 500),
    Transfer("tr4", "acc1", "acc5", 500)
  )

  protected final val transferService =
    TransferDoobieService(
      TransferRepository(),
      accountService
    )

  protected final def initTransfers(): IO[Unit] =
    initAccounts() >>
      testTransfers
        .traverse(transferService.create)
        .transact(transactorIO)
        .value
        .void
}
