package fp.transaction.transfer

import cats.effect.IO
import cats.syntax.bifunctor._
import cats.syntax.flatMap._
import cats.syntax.semigroupal._
import doobie.syntax.connectionio._
import doobie.{ConnectionIO, Transactor}
import fp.DoobieServiceTestUtils
import fp.domain.Account
import fp.domain.Transaction.Transfer
import fp.domain.validation.account._
import fp.domain.validation.syntax._
import fp.domain.validation.transfer._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

final class TransferServiceSpec
    extends AnyFunSuite
    with Matchers
    with BeforeAndAfterAll
    with TransferServiceTestUtils {

  private val xa: Transactor[IO] = DoobieServiceTestUtils.rollbackTransactor

  override def beforeAll(): Unit =
    initTransfers().unsafeRunSync()

  test("create a new transfer") {
    val transfer = Transfer("toCreate", "acc2", "acc1", 700)

    def getAccounts: (Account, Account) |! ConnectionIO =
      accountService
        .getById(transfer.fromId)
        .product(accountService.getById(transfer.toId))
        .leftWiden

    (
      for {
        (fromAccBefore, toAccBefore) <- getAccounts
        _                            <- transferService.create(transfer)
        (fromAccAfter, toAccAfter)   <- getAccounts
      } yield {
        (
          fromAccBefore.balance - fromAccAfter.balance,
          toAccAfter.balance - toAccBefore.balance
        )
      }
    ).transact(xa)
      .value
      .map {
        _.map { case (fromAccDelta, toAccDelta) =>
          fromAccDelta shouldEqual transfer.amount
          toAccDelta shouldEqual transfer.amount
        }
      }
      .unsafeRunSync()
  }

  test("create a transfer with existing id") {
    val transfer = testTransfers.head
    transferService
      .create(transfer)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          TransferAlreadyExistsError(transfer.id)
        )
      }
      .unsafeRunSync()
  }

  test("create a transfer with a negative amount") {
    val transfer = Transfer("toCreate", "acc2", "acc5", -100)
    transferService
      .create(transfer)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          TransferNonPositiveAmountError
        )
      }
      .unsafeRunSync()
  }

  test("create a transfer from a non-existing account") {
    val transfer = Transfer("toCreate", "unknown_id", "acc5", 900)
    transferService
      .create(transfer)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          AccountNotFoundError(transfer.fromId)
        )
      }
      .unsafeRunSync()
  }

  test("create a transfer to a non-existing account") {
    val transfer = Transfer("toCreate", "acc2", "unknown_id", 900)
    transferService
      .create(transfer)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          AccountNotFoundError(transfer.toId)
        )
      }
      .unsafeRunSync()
  }

  test("get an existing transfer") {
    val transfer = testTransfers(1)
    transferService
      .getById(transfer.id)
      .transact(xa)
      .value
      .map(_ shouldEqual Right(transfer))
      .unsafeRunSync()
  }

  test("get a non-existing transfer") {
    transferService
      .getById("unknown_id")
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          TransferNotFoundError("unknown_id")
        )
      }
      .unsafeRunSync()
  }

  test("get all transfers") {
    transferService.list
      .transact(xa)
      .map {
        _ should contain theSameElementsAs testTransfers
      }
      .unsafeRunSync()
  }

  test("update an existing transfer") {
    val transfer        = testTransfers(2)
    val updatedTransfer = transfer.copy(amount = transfer.amount * 2)
    (
      transferService.updatePure(transfer.id, _ => updatedTransfer) >>
        transferService.getById(updatedTransfer.id).leftWiden
    ).transact(xa)
      .value
      .map {
        _ shouldEqual Right(updatedTransfer)
      }
      .unsafeRunSync()
  }

  test("update a non-existing transfer") {
    val unknownId = "toUpdate"

    transferService
      .updatePure(unknownId, identity)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          TransferNotFoundError(unknownId)
        )
      }
      .unsafeRunSync()
  }

  test("delete an existing transfer") {
    transferService
      .delete(testTransfers(3).id)
      .value
      .transact(xa)
      .map(_ shouldEqual Right(1))
      .unsafeRunSync()
  }
}
