package fp.user

import cats.effect.IO
import cats.syntax.traverse._
import doobie.syntax.connectionio._
import fp.DoobieServiceTestUtils
import fp.basic.doobie.BasicDoobieService
import fp.domain.User

trait UserServiceTestUtils {

  protected final val testUsers = List(
    User("u1", "email1"),
    User("u2", "email2"),
    User("u3", "email3"),
    User("u4", "email4"),
    User("u5", "email5"),
    User("u6", "email6")
  )

  protected final val userService: BasicDoobieService[User] =
    BasicDoobieService(UserRepository())

  protected final def initUsers(): IO[Unit] =
    testUsers
      .traverse(userService.create)
      .transact(DoobieServiceTestUtils.transactorIO)
      .value
      .void
}
