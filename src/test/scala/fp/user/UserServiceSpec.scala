package fp.user

import cats.effect.IO
import cats.syntax.bifunctor._
import cats.syntax.flatMap._
import doobie.Transactor
import doobie.syntax.connectionio._
import fp.DoobieServiceTestUtils
import fp.domain.User
import fp.domain.validation.user._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

final class UserServiceSpec
    extends AnyFunSuite
    with Matchers
    with BeforeAndAfterAll
    with UserServiceTestUtils {

  private val xa: Transactor[IO] = DoobieServiceTestUtils.rollbackTransactor

  override def beforeAll(): Unit =
    initUsers().unsafeRunSync()

  test("create a new user") {
    val user = User("toCreate", "some_email")
    (
      userService.create(user) >>
        userService.getById(user.id).leftWiden
    )
      .transact(xa)
      .value
      .map(_ shouldEqual Right(user))
      .unsafeRunSync()
  }

  test("create a user with an existing id") {
    val user = testUsers.head
    userService
      .create(user)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          UserAlreadyExistsError(user.id)
        )
      }
      .unsafeRunSync()
  }

  test("get an existing user") {
    val user = testUsers(1)
    userService
      .getById(user.id)
      .transact(xa)
      .value
      .map(_ shouldEqual Right(user))
      .unsafeRunSync()
  }

  test("get a non-existing user") {
    userService
      .getById("unknown_id1")
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          UserNotFoundError("unknown_id1")
        )
      }
      .unsafeRunSync()
  }

  test("get all users") {
    userService.list
      .transact(xa)
      .map {
        _ should contain theSameElementsAs testUsers
      }
      .unsafeRunSync()
  }

  test("update an existing user") {
    val user        = testUsers(2)
    val updatedUser = user.copy(email = user.email.reverse)
    (
      userService.updatePure(user.id, _ => updatedUser) >>
        userService.getById(user.id).leftWiden
    )
      .transact(xa)
      .value
      .map(_ shouldEqual Right(updatedUser))
      .unsafeRunSync()
  }

  test("update a non-existing user") {
    val unknownId = "toUpdate"

    userService
      .updatePure(unknownId, identity)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          UserNotFoundError(unknownId)
        )
      }
      .unsafeRunSync()
  }

  test("delete an existing user") {
    userService
      .delete(testUsers(5).id)
      .transact(xa)
      .value
      .map(_ shouldEqual Right(1))
      .unsafeRunSync()
  }
}
