package fp.account

import cats.effect.IO
import cats.syntax.flatMap._
import cats.syntax.traverse._
import doobie.syntax.connectionio._
import fp.DoobieServiceTestUtils.transactorIO
import fp.domain.Account
import fp.user.UserServiceTestUtils

trait AccountServiceTestUtils extends UserServiceTestUtils {

  protected final val testAccounts = List(
    Account("acc1", "u1", 1000),
    Account("acc2", "u2", 2000),
    Account("acc3", "u3", 1500),
    Account("acc4", "u4", 2000),
    Account("acc5", "u5", 1000),
    Account("acc6", "u5", 0)
  )

  protected final val accountService: AccountDoobieService =
    AccountDoobieService(
      AccountRepository(),
      userService
    )

  protected final def initAccounts(): IO[Unit] =
    initUsers() >>
      testAccounts
        .traverse(accountService.create)
        .transact(transactorIO)
        .value
        .void
}
