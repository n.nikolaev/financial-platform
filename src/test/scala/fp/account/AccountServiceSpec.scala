package fp.account

import cats.effect.IO
import cats.syntax.bifunctor._
import cats.syntax.flatMap._
import doobie.Transactor
import doobie.syntax.connectionio._
import fp.DoobieServiceTestUtils
import fp.domain.Account
import fp.domain.validation.account._
import fp.domain.validation.user._
import org.scalatest.BeforeAndAfterAll
import org.scalatest.funsuite.AnyFunSuite
import org.scalatest.matchers.should.Matchers

final class AccountServiceSpec
    extends AnyFunSuite
    with Matchers
    with BeforeAndAfterAll
    with AccountServiceTestUtils {

  private val xa: Transactor[IO] = DoobieServiceTestUtils.rollbackTransactor

  override def beforeAll(): Unit =
    initAccounts().unsafeRunSync()

  test("create a new account") {
    val account = Account("toCreate", testUsers.head.id, 500)
    (
      accountService.create(account) >>
        accountService.getById(account.id).leftWiden
    )
      .transact(xa)
      .value
      .map(_ shouldEqual Right(account))
      .unsafeRunSync()
  }

  test("create an account with existing id") {
    val account = testAccounts.head
    accountService
      .create(account)
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          AccountAlreadyExistsError(account.id)
        )
      }
      .unsafeRunSync()
  }

  test("create an account with non-existing user") {
    val account = Account("toCreate", "unknown_id1", 500)
    accountService
      .create(account)
      .transact(xa)
      .value
      .map(
        _ shouldEqual Left(
          UserNotFoundError(account.userId)
        )
      )
      .unsafeRunSync()
  }

  test("get an existing account") {
    val account = testAccounts(1)
    accountService
      .getById(account.id)
      .transact(xa)
      .value
      .map(_ shouldEqual Right(account))
      .unsafeRunSync()
  }

  test("get a non-existing account") {
    accountService
      .getById("unknown_id2")
      .transact(xa)
      .value
      .map {
        _ shouldEqual Left(
          AccountNotFoundError("unknown_id2")
        )
      }
      .unsafeRunSync()
  }

  test("get all accounts") {
    accountService.list
      .transact(xa)
      .map {
        _ should contain theSameElementsAs testAccounts
      }
      .unsafeRunSync()
  }

  test("update an existing account") {
    val account    = testAccounts(2)
    val updatedAcc = account.copy(balance = account.balance + 400)
    (
      accountService.updatePure(account.id, _ => updatedAcc) >>
        accountService.getById(updatedAcc.id).leftWiden
    )
      .transact(xa)
      .value
      .map(_ shouldEqual Right(updatedAcc))
      .unsafeRunSync()
  }

  test("update a non-existing account") {
    val unknownId = "toUpdate"

    accountService
      .updatePure(unknownId, identity)
      .transact(xa)
      .value
      .map(
        _ shouldEqual Left(
          AccountNotFoundError(unknownId)
        )
      )
      .unsafeRunSync()
  }

  test("concurrent increase balance") {
    import cats.effect.ContextShift
    import cats.syntax.parallel._
    import fp.domain.validation.ValidationError

    import scala.concurrent.ExecutionContext

    implicit val contextShift: ContextShift[IO] =
      IO.contextShift(ExecutionContext.global)

    val xaIO = DoobieServiceTestUtils.transactorIO

    val initialAccount = Account("acc100", "u1", 100)
    val deltas         = List(31, 32, 33, 34)

    def io(delta: BigDecimal): IO[Either[ValidationError, Int]] =
      accountService
        .increaseBalance(initialAccount.id, delta)
        .transact(xaIO)
        .value

    {
      for {
        _   <- accountService
                 .create(initialAccount)
                 .transact(xaIO)
                 .value
        _   <- deltas.map(io(_)).parSequence
        res <- accountService
                 .getById(initialAccount.id)
                 .transact(xaIO)
                 .value
      } yield {
        res.map { updatedAccount =>
          updatedAccount.balance shouldEqual initialAccount.balance + deltas.sum
        }
      }
    }.unsafeRunSync()
  }

  test("delete an existing account") {
    accountService
      .delete(testAccounts(5).id)
      .value
      .transact(xa)
      .map(_ shouldEqual Right(1))
      .unsafeRunSync()
  }

  test("increase balance") {
    val delta         = 650
    val accountBefore = testAccounts(4)
    (
      accountService.increaseBalance(accountBefore.id, delta) >>
        accountService.getById(accountBefore.id).leftWiden
    )
      .transact(xa)
      .value
      .map {
        _.map { accountAfter =>
          accountAfter.balance shouldEqual accountBefore.balance + delta
        }
      }
      .unsafeRunSync()
  }

  test("decrease balance") {
    val delta         = 350
    val accountBefore = testAccounts(4)
    (
      accountService.decreaseBalance(accountBefore.id, delta) >>
        accountService.getById(accountBefore.id).leftWiden
    )
      .transact(xa)
      .value
      .map {
        _.map { accountAfter =>
          accountAfter.balance shouldEqual accountBefore.balance - delta
        }
      }
      .unsafeRunSync()
  }
}
