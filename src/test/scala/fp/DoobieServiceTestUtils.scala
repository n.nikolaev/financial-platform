package fp

import java.util.concurrent.Executors

import cats.effect.{ContextShift, IO}
import doobie.Transactor

import scala.concurrent.ExecutionContext

object DoobieServiceTestUtils {

  private implicit val cs: ContextShift[IO] = IO.contextShift(
    ExecutionContext.fromExecutor(Executors.newCachedThreadPool)
  )

  val transactorIO: Transactor[IO] = Transactor.fromDriverManager[IO](
    "org.h2.Driver",
    "jdbc:h2:mem:fp-test;MODE=PostgreSQL;" +
      "INIT=runscript from 'src/main/resources/migrations/db_init/1-setup.sql';" +
      "DB_CLOSE_DELAY=-1",
    "user1",
    "pwd"
  )

  val rollbackTransactor: Transactor[IO] =
    Transactor.after.set(transactorIO, doobie.HC.rollback)
}
