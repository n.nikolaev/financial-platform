addSbtPlugin("com.eed3si9n"           % "sbt-assembly"       % "0.15.0")
addSbtPlugin("se.marcuslonnberg"      % "sbt-docker"         % "1.8.0")
addSbtPlugin("org.duhemm"             % "sbt-errors-summary" % "0.6.3")
addSbtPlugin("io.spray"               % "sbt-revolver"       % "0.9.1")
addSbtPlugin("com.sksamuel.scapegoat" % "sbt-scapegoat"      % "1.1.0")
addSbtPlugin("org.wartremover"        % "sbt-wartremover"    % "2.4.13")

// Informational plugins
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph"      % "0.10.0-RC1")
addSbtPlugin("com.github.cb372" % "sbt-explicit-dependencies" % "0.2.15")
addSbtPlugin("com.orrsella"     % "sbt-stats"                 % "1.0.7")
addSbtPlugin("com.timushev.sbt" % "sbt-updates"               % "0.5.1")
