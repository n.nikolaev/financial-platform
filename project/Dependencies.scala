import sbt._

object Dependencies {
  object Versions {
    lazy val cats            = "2.2.0"
    lazy val circe           = "0.13.0"
    lazy val circeConfig     = "0.8.0"
    lazy val doobie          = "0.9.2"
    lazy val enumeratumCirce = "1.6.1"
    lazy val http4s          = "0.21.9"
    lazy val slf4j           = "1.7.30"
  }

  lazy val all = Seq.concat(
    catsDependencies,
    circeDependencies,
    circeConfigDependencies,
    dbDependencies,
    enumeratumDependencies,
    http4sDependencies,
    slf4jDependencies,
    testDependencies
  )

  val catsDependencies   = Seq(
    "org.typelevel" %% "cats-core",
    "org.typelevel" %% "cats-effect"
  ).map(_ % Versions.cats)

  val doobieDependencies = Seq(
    "org.tpolecat" %% "doobie-core",
    "org.tpolecat" %% "doobie-free",
    "org.tpolecat" %% "doobie-hikari"
  ).map(_ % Versions.doobie)

  val dbDependencies     = Seq(
    "com.zaxxer"     % "HikariCP"   % "3.4.5",
    "org.postgresql" % "postgresql" % "42.2.18" % Runtime
  ) ++ doobieDependencies

  val circeDependencies       = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic"
  ).map(_ % Versions.circe)

  val circeConfigDependencies = Seq(
    "io.circe" %% "circe-config" % Versions.circeConfig
  )

  val enumeratumDependencies = Seq(
    "com.beachape" %% "enumeratum-circe" % Versions.enumeratumCirce
  )

  val slf4jDependencies = Seq(
    "org.slf4j" % "slf4j-simple" % Versions.slf4j % Runtime
  )

  val http4sDependencies = Seq(
    "org.http4s" %% "http4s-blaze-server",
    "org.http4s" %% "http4s-circe",
    "org.http4s" %% "http4s-core",
    "org.http4s" %% "http4s-dsl",
    "org.http4s" %% "http4s-server"
  ).map(_ % Versions.http4s)

  val testDependencies   = Seq(
    "com.h2database" % "h2"        % "1.4.200",
    "org.scalatest" %% "scalatest" % "3.2.3"
  ).map(_ % Test)
}
