import sbt.Keys.{scalaVersion, version}

addCommandAlias(
  "checkDeps",
  ";undeclaredCompileDependencies;unusedCompileDependencies"
)

lazy val `financial-platform` = project
  .in(file("."))
  .enablePlugins(DockerPlugin)
  .settings(compilerPlugins)
  .settings(
    Seq.concat(
      basicSettings,
      pluginsSettings,
      testSettings,
      assemblySettings,
      dockerSettings
    )
  )
  .settings(libraryDependencies ++= Dependencies.all)

lazy val compilerPlugins = Seq(
  addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.3.1"),
  addCompilerPlugin("io.tryp"     % "splain"             % "0.5.7" cross CrossVersion.patch),
  addCompilerPlugin(
    "org.typelevel"              %% "kind-projector"     % "0.11.1" cross CrossVersion.full
  )
)

lazy val basicSettings = Seq(
  ThisBuild / name := "financial-platform",
  ThisBuild / version := "0.1",
  ThisBuild / scalaVersion := "2.13.3"
)

lazy val pluginsSettings = Seq(
  Compile / compile / wartremoverWarnings ++=
    Warts.allBut(Wart.Any, Wart.Nothing),
  ThisBuild / scapegoatVersion := "1.4.6",
  scapegoatConsoleOutput := false,
  scalacOptions ++= Seq(
    "-P:splain:all:true",
    "-P:splain:tree:false"
  )
)

lazy val testSettings = Seq(
  Test / fork := true
)

lazy val assemblySettings = Seq(
  assembly / assemblyJarName := s"${name.value}_${version.value}.jar",
  assembly / mainClass := Some("fp.Server")
)

lazy val dockerSettings = Seq(
  docker := (docker dependsOn assembly).value,
  docker / buildOptions := BuildOptions(
    cache = false,
    removeIntermediateContainers = BuildOptions.Remove.Always
  ),
  docker / dockerfile := {
    val workDir            = s"/${name.value}"
    val artifact: File     = assembly.value
    val artifactTargetPath = s"$workDir/${artifact.name}"

    new Dockerfile {
      from("openjdk:11.0.9.1-jre")
      copy(artifact, artifactTargetPath)
      entryPoint("java", "-jar", artifactTargetPath)
    }
  },
  docker / imageNames := Seq(ImageName(s"${name.value}:${version.value}"))
)
