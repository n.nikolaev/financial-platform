# Financial platform

## Description

The project implements a backend of a web application 
that provides a set of operations to perform financial 
transactions between user accounts.

### Domain entities:
- Users
- Accounts
- Transactions:
    - Transfers
    - Payments

## Stack

* Libraries:
    - [cats](https://github.com/typelevel/cats) - FP toolkit
    - [circe](https://github.com/circe/circe) - JSON encoding/decoding
    - [circe-config](https://github.com/circe/circe-config) - App config
    - [doobie](https://github.com/tpolecat/doobie) - Interaction with DB
    - [http4s](https://github.com/http4s/http4s) - Web server
    - [scalatest](https://github.com/scalatest/scalatest) - Testing
* Tools:
    - [H2](https://github.com/h2database/h2database) - In-memory DB for testing
    - [PostgreSQL](https://www.postgresql.org/) - Main relational DB
    - [Docker](https://www.docker.com/) - Application containerization
